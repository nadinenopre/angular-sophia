import { State } from '../shared/enums/state.enum';
import { Item } from '../shared/models/item.model';

export const COLLECTION: Item[] = [
  {
    id: 'a1',
    name: ' Christophe',
    reference: '1235',
    state: State.ALIVRER
    },
  {
  id: 'b1',
  name: 'Nadine',
  reference: '2546',
  state: State.ENCOURS
  },
 {
    id: 'C1',
    name: 'Marc',
    reference: '5522',
    state: State.LIVREE
    }
]
