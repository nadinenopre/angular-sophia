import { Injectable } from '@angular/core';
import { Item } from '../../../shared/models/item.model';
import { COLLECTION } from '../../collection';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CollectionService {

  itemsCollection: AngularFirestoreCollection<Item>;
  private _collection: Observable<Item[]>;

  constructor(
    private afs: AngularFirestore,
    // private http: HttpClient
  ) {
    this.itemsCollection = afs.collection<Item>('collection');
    this._collection = this.itemsCollection.valueChanges();
    // this._collection = this.http.get('url-api/collection');
   }
/**
 * get collection
 */

 get collection(): Observable<Item[]> {
   return this._collection;
   // return this.http.get('url-api/collection');
 }

 /**
 * set collection
 */

set collection(collection: Observable<Item[]>) {
  this._collection = collection;

 }

 add(item: Item): void {
  item.id = this.afs.createId();  // cree 1 doc avec une clé id
  this.itemsCollection.doc(item.id).set(item)
    .catch(error => console.log(error));
}

getItem(id: string): Observable<Item> {
  const item = this.afs.doc<Item>(`collection/${id}`).valueChanges();
  return item;
}

update(item: Item): void {
  this.itemsCollection.doc(item.id).update(item)
    .catch(error => console.log(error));
}

delete(item: Item): void {
  this.itemsCollection.doc(item.id).delete()
    .catch(error => console.log(error));
}

}
