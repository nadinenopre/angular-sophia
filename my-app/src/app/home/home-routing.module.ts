import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },  // il prend la 1ere qui matche
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      appRoutes,
      // { enableTracing: true }  <-- debugging purposes only
    )
  ],
  declarations: []
})
export class HomeRoutingModule { }
