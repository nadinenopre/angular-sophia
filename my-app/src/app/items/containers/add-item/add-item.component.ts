import { Component, OnInit } from '@angular/core';
import { Item } from '../../../shared/models/item.model';
import { CollectionService } from '../../../core/services/collection/collection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {

  constructor(
    private collectionService: CollectionService,
    private router: Router      // on injecte le router il n'y a qu'1 seule instante pour l'appli
) { }

  ngOnInit() {
  }

  addItem(item: Item) {
    console.log(item);
    this.collectionService.add(item);
    this.router.navigate(['/items/list']);  // this.router.navigate(['/items/list', item.id]); si l'on veut passer un paramètre
                                            // prévoir route avec list/:id
  }
}
