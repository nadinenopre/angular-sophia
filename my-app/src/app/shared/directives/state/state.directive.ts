import { Directive, OnInit, Input, HostBinding, OnChanges } from '@angular/core';
import { State } from '../../enums/state.enum';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnChanges {
  [x: string]: any;
  @Input() appState: State;
  @HostBinding('class') nomClass: string;

  constructor() {
  }

  ngOnChanges() {
    console.log(this.appState);
    this.nomClass = this.formatClass(this.appState);
  }

  private removeAccent(etat: string): string {
   //https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript
    return etat.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }

  private formatClass(chaine: string): string {
    return `state-${this.removeAccent(chaine)
    .toLowerCase()
    .replace(' ', '')}
    `;
  }

}
