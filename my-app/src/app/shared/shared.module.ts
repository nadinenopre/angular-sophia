import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/nav/nav.component';
import { ItemComponent } from './components/item/item.component';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { StateDirective } from './directives/state/state.directive';
import { FormComponent } from './form/form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    NgbCollapseModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [NavComponent, ItemComponent, StateDirective, FormComponent],
  exports: [NavComponent,
            ItemComponent,
            StateDirective,
            FormComponent
          ]
})
export class SharedModule { }
